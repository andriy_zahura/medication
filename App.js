import {NavigationContainer} from '@react-navigation/native'
import {configure} from 'mobx'
import React from 'react'
import Navigation from './src/navigation'
import {navigationRef} from './src/navigation/RootNavigation'

configure({enforceActions: false})

export default function App() {
  return (
    <NavigationContainer ref={navigationRef} st>
      <Navigation />
    </NavigationContainer>
  )
}
