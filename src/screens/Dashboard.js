
import {observer} from 'mobx-react-lite'
import React from 'react'
import {Image, ScrollView, StyleSheet, View} from 'react-native'
import ContentHeader from '../components/ContentHeader'
import Footer from '../components/Footer'
import Header from '../components/Header'
import HeaderText from '../components/HeaderText'
import Message from '../components/Message'
import authStore from '../store/auth.store'
import dashboardStore from '../store/Dashboard.store'

export default observer(() => {
  return (
    <View
      style={styles.container}
    >
      <View>
        <Header currentScreen="Dashboard" />
        <View style={styles.header}>
          <Image
            source={{uri: authStore.user.image}}
            style={styles.avatar}
          />
          <View style={styles.headerTextContainer}>
            <HeaderText text={dashboardStore.daytime} />
            <HeaderText text={authStore.user.name} />
          </View>
        </View>
        <View style={styles.contentContainer}>
          <ContentHeader title="Messages" />
          <ScrollView style={{height: '50%'}} showsVerticalScrollIndicator={false}>
            {dashboardStore.messages.map(({text, date, checked, id}) => {
              return (
                <Message text={text} date={date} key={id} checked={checked} />
              )
            })}
          </ScrollView>
        </View>
      </View>
      <Footer currentScreen="dashboard" />
    </View>
  )
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between'
  },
  header: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginBottom: 30
  },
  avatar: {
    width: 80,
    height: 80,
    borderRadius: 40,
    margin: 10
  },
  headerTextContainer: {
    alignItems: 'center'
  },
  contentContainer: {
    paddingRight: 24,
    paddingLeft: 24,
  },
})
