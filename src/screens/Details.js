
import {observer, useLocalObservable} from 'mobx-react-lite'
import React, {useEffect} from 'react'
import {Dimensions, Image, ScrollView, StyleSheet, View} from 'react-native'
import Header from '../components/Header'
import HeaderText from '../components/HeaderText'
import Icon from '../components/Icon'
import medicationStore from '../store/Medication.store'

export default observer(({route}) => {
  const store = useLocalObservable(() => ({
    item: {...medicationStore.items.find(med => med.id === route.params.id)},
    val: false,
  }))

  useEffect(() => {
    if (store.item) {
      store.val = store.item.checked
    }
  }, [store.item])

  return (
    <ScrollView contentContainerStyle={styles.flexContainer}>
      <Header currentScreen="Details" isDetails />
      <HeaderText style={styles.text} text={`${store.item.text}` || ''} />
      <View style={styles.flexContainer}>
        <HeaderText style={styles.text} text={`${store.item.date}` || ''} />
        {store.val && (
          <Icon
            type="ionic"
            name="checkbox-outline"
            size={50} color="#9FA2B2"
            onPress={() => {
              medicationStore.checkElement(route.params.id)
              store.val = !store.val
            }}
          />
        )}
        {!store.val && (
          <Icon
            type="ionic"
            name="md-square-outline"
            size={50}
            color="#9FA2B2"
            onPress={() => {
              medicationStore.checkElement(route.params.id)
              store.val = !store.val
            }}
          />
        )}
      </View>
      <Image source={{uri: store.item.img || ''}} style={styles.image} />
    </ScrollView>
  )
})

const styles = StyleSheet.create({
  flexContainer: {
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  text: {
    fontSize: 56,
    letterSpacing: 2,
    paddingLeft: 24,
    paddingRight: 24,
    marginTop: 20
  },
  image: {
    borderRadius: 5,
    marginTop: 20,
    height: 300,
    width: Dimensions.get('window').width * 0.8,
    resizeMode: 'contain',
  }
})