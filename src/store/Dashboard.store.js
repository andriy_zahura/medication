import {observable} from "mobx"

const store = observable({
  messages: [
    {
      id: 1,
      date: '15.09.2021',
      text: 'First message'
    },
    {
      id: 2,
      date: '14.09.2021',
      text: 'Second message'
    },
    {
      id: 3,
      date: '13.09.2021',
      text: 'Third message'
    },
  ],
  get daytime() {
    const time = new Date()
    const hours = time.getHours()
    if (hours < 5) {
      return 'Good night,'
    } else if (5 < hours && hours < 12) {
      return 'Good morning,'
    } else if (12 < hours && hours < 18) {
      return 'Good day,'
    } else {return 'Good evening,'}
  }
})

export default store