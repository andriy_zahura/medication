import {observable} from "mobx"

const store = observable({
  items: [
    {
      id: 20,
      date: '12:03',
      text: 'Analgin',
      checked: true,
      img: 'https://www.vidal.ru/upload/products/analgin-10tab-avexima.jpg'
    },
    {
      id: 35,
      date: '09:00',
      text: 'Aspirin',
      checked: false,
      img: 'https://d2d8wwwkmhfcva.cloudfront.net/800x/d2lnr5mha7bycj.cloudfront.net/product-image/file/large_60c0ef61-b7eb-47fd-a3b9-9eab521b3355.png'
    },
    {
      id: 412,
      date: '07:00',
      text: 'B12',
      checked: true,
      img: 'https://www.chatelaine.com/wp-content/uploads/2017/04/B12-feature-1-660x495.jpg'
    },
  ],
  checkElement(id) {
    const element = this.items.find(el => {
      return el.id === id
    })
    this.items[this.items.indexOf(element)].checked = !this.items[this.items.indexOf(element)].checked
  }
})

export default store