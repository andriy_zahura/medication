
import {observer, useLocalObservable} from 'mobx-react-lite'
import React from 'react'
import {Calendar} from 'react-native-calendars'

export default observer(() => {
  const store = useLocalObservable(() => ({
    get today() {
      const today = new Date()
      return today.toISOString().split('T')[0]
    }
  }))

  return (
    <Calendar
      markingType="period"
      current={store.today}
      markedDates={{[store.today]: {color: '#2078E5', selected: true}}}
    />
  )
})