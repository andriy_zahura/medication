
import {observer} from 'mobx-react-lite'
import React from 'react'
import {StyleSheet, Text} from 'react-native'

export default observer(({text, style}) => {
  return (
    <Text style={{...styles.text, ...style}}>{text.toUpperCase()}</Text>
  )
})

const styles = StyleSheet.create({
  text: {
    fontSize: 28,
    letterSpacing: 3,
    textAlign: 'center',
    fontWeight: '400',
    color: '#042C5C',
  }
})