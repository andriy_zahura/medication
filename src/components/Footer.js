
import {observer, useLocalObservable} from 'mobx-react-lite'
import React from 'react'
import {Pressable, StyleSheet} from 'react-native'
import {navigate} from '../navigation/RootNavigation'
import Icon from './Icon'

export default observer(({currentScreen}) => {
  const store = useLocalObservable(() => ({
    currentScreen
  }))
  return (
    <Pressable style={styles.container}>
      <Pressable
        style={store.currentScreen === 'dashboard' ? styles.pressableChosen : styles.pressable}
        onPress={() => navigate('dashboard')}
      >
        <Icon type="ionic" name="menu" size={30} color="#9FA2B2" />
      </Pressable>
      <Pressable
        style={store.currentScreen === 'medication' ? styles.pressableChosen : styles.pressable}
        onPress={() => navigate('medication')}
      >
        <Icon name="medical-services" size={30} color="#9FA2B2" />
      </Pressable>
      <Pressable
        style={store.currentScreen === 'calendar' ? styles.pressableChosen : styles.pressable}
        onPress={() => navigate('calendar')}
      >
        <Icon type="ionic" name="calendar-sharp" size={30} color="#9FA2B2" />
      </Pressable>
    </Pressable>
  )
})

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    maxHeight: 100
  },
  pressable: {
    flex: 0.33,
    borderWidth: 1,
    borderColor: '#000',
    alignItems: 'center'
  },
  pressableChosen: {
    flex: 0.33,
    backgroundColor: '#90D5F6',
    borderWidth: 1,
    alignItems: 'center',
    borderColor: '#000'
  },
})