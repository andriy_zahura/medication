
import {observer} from 'mobx-react-lite'
import React from 'react'
import {StyleSheet, Text} from 'react-native'

export default observer(({text, style}) => {
  return (
    <Text style={{...style, ...styles.text}}>{text}</Text>
  )
})

const styles = StyleSheet.create({
  text: {
    fontSize: 22,
    fontFamily: 'monospace',
    fontWeight: '400',
    color: '#000',
  }
})