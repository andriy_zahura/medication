
import {observer} from 'mobx-react-lite'
import React from 'react'
import {StyleSheet, View} from 'react-native'
import ParahraphText from './ParahraphText'

export default observer(({text = '', date = ''}) => {
  return (
    <View style={styles.container}>
      <ParahraphText text={date} />
      <ParahraphText text={text} />
    </View>
  )
})

const styles = StyleSheet.create({
  container: {
    height: 100,
    fontSize: 24,
    shadowColor: 'rgba(43, 108, 192, 0.2)',
    shadowOffset: {width: 0, height: 10},
    shadowOpacity: 0.8,
    elevation: 3,
    marginBottom: 10,
  }
})
