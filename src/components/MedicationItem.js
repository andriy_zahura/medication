
import {observer} from 'mobx-react-lite'
import React from 'react'
import {Pressable, StyleSheet} from 'react-native'
import {navigate} from '../navigation/RootNavigation'
import medicationStore from '../store/Medication.store'
import Icon from './Icon'
import ParahraphText from './ParahraphText'

export default observer(({date, text, checked, id}) => {
  return (
    <Pressable style={styles.container} onPress={() => navigate('details', {id})}>
      <ParahraphText text={date} />
      <ParahraphText text={text} />
      {checked && (
        <Icon
          type="ionic"
          name="checkbox-outline"
          size={30} color="#9FA2B2"
          onPress={() => medicationStore.checkElement(id)} />
      )}
      {!checked && (
        <Icon
          type="ionic"
          name="md-square-outline"
          size={30}
          color="#9FA2B2"
          onPress={() => medicationStore.checkElement(id)}
        />
      )}
    </Pressable>
  )
})

const styles = StyleSheet.create({
  container: {
    height: 100,
    fontSize: 24,
    shadowColor: 'rgba(43, 108, 192, 0.2)',
    shadowOffset: {width: 0, height: 10},
    shadowOpacity: 0.8,
    elevation: 3,
    marginBottom: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center'
  }
})
